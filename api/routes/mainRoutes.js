const express = require('express');
const router = express.Router();
var crypto = require('crypto');

algorithm = 'aes-256-ctr'

router.get('/', (req, res) => {
	res.status(200).send('Hi.');
});

router.post('/encrypt', (req, res) => {
	var password = req.body.password;
	var text = req.body.text;
	console.log(JSON.stringify(req.body));
	console.log('text is ' + text + ' and password is ' + password);
	var encrypted = encrypt(text, password);
	res.status(200).send(encrypted);
});

router.post('/decrypt', (req, res) => {
	var password = req.body.password;
	var encrypted = req.body.encrypted;
	var decrypted = decrypt(encrypted, password);
	res.status(200).send(decrypted);
});

function encrypt(text, password){
  var cipher = crypto.createCipher(algorithm,password)
  var crypted = cipher.update(text,'utf8','hex')
  crypted += cipher.final('hex');
  return crypted;
}

function decrypt(text, password){
  var decipher = crypto.createDecipher(algorithm,password)
  var dec = decipher.update(text,'hex','utf8')
  dec += decipher.final('utf8');
  return dec;
}

module.exports = router;
